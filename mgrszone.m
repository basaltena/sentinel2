function [ MGRSstring ] = mgrszone( LAT, LON )
%MGRSZONE military grid reference system
%   ZONE = MGRSZONE(LAT, LON) returns a military grid reference system zone 
%   designation string. This zoneing is used in the tiling of Sentinel. 
%   The points specified by coordinates LAT and LON are in units of degrees.
%
%   December 2015 - B.Altena

% numbering goes with the alphabet, excluding "O" and "I"
mgrsABC = char([65:72 74:78 80:90]); % 24 characters
tileSize = 100; % [km] stretch of a tile

% estimate utm zone
zone = utmzone2(LAT, LON);
zoneNum = str2double(zone(1:2));

% construct map structure
utmstruct = defaultm('utm'); 
if LAT<0 % Northern or Southern Hemisphere
    utmstruct.zone = [zone(1:2) 'S']; 
else
    utmstruct.zone = [zone(1:2) 'N']; 
end
utmstruct = defaultm(utmstruct);
Ell = referenceEllipsoid('wgs84','meters');
utmstruct.geoid = [Ell.SemimajorAxis Ell.Eccentricity];

[xUTM,yUTM] = mfwdtran(utmstruct, LAT, LON);
clear Ell
%% extract longnitude letter
shiftLetter = floor((((xUTM-utmstruct.falseeasting)+eps)./1e3)./tileSize);
centerLetter = (mod(zoneNum-1,3).*8)+5;

lonLetter = mgrsABC(centerLetter + shiftLetter);
clear shiftLetter centerLetter
%% extract latitude letter
tileShift = fix((yUTM./1e3)./tileSize);
latNum = mod(tileShift,20);

if ~mod(zoneNum,2) % even get additional 5 letter shift
    if yUTM<0 && abs(tileShift)>5
        latNum = latNum - 4; % counts up to R
    end
    latNum = latNum + 5;
else
    if yUTM<0 % a leap hole is present in the southern hemisphere
        latNum = latNum - 4; % counts up to R
    end
end
latNum = mod(latNum, 20);
if latNum == 0
    latNum = 20;
end
latLetter = mgrsABC(latNum);

%% catenate all strings
MGRSstring = [zone lonLetter latLetter];
end

function varargout = utmzone2(varargin)
%UTMZONE  Select UTM zone given latitude and longitude
%
%   ZONE = UTMZONE selects a Universal Transverse Mercator (UTM) zone with
%   a graphical user interface.
%
%   ZONE = UTMZONE(LAT, LON) returns a UTM zone designation string for the
%   zone containing the points specified by coordinates LAT and LON, in
%   units of degrees.  If LAT and LON are vectors, the zone containing the
%   geographic mean of the data set is determined.
%
%   [LATLIM, LONLIM] = UTMZONE(ZONE), where ZONE is valid UTM zone
%   designation string, returns the geographic limits of the zone, LATLIM
%   and LONLIM, in units of degrees.  Valid UTM zones designations are
%   numbers (such as '31') , or numbers followed by a single letter (such
%   as '31L'.
%
%   Upgrade
%   -----------
%   The UTM zone system is based on a regular division of the globe, with
%   the exception of a few zones in northern Europe.  utmzone2 does
%   account for these exceptions, which involve zones 31-32 V (southern
%   Norway) and zones 31-37 X (Svalbard).
%
%   See also UTMZONEUI, UTMGEOID.

% Copyright 1996-2012 The MathWorks, Inc.

% Syntaxes to be removed. These are supported for now, but not encouraged.
%
%   ZONE = UTMZONE(MAT), where MAT has the form [LAT LON].
%   LIM = UTMZONE(ZONE) returns the limits in a single 1-by-4 vector.

narginchk(0, 2)
if nargin == 0
    nargoutchk(0, 1)
    
    if nargout < 2
        % ZONE = UTMZONE();
        varargout{1} = utmzoneui;
    else
        % [ZONE, MSG] = UTMZONE();
        varargout{1} = utmzoneui;
        varargout{2} = '';
        warnObsoleteMSGSyntax
    end
elseif ischar(varargin{1})
    narginchk(1, 1)
    nargoutchk(0, 3)
    
    zone = upper(varargin{1});
    [latlim, lonlim] = zoneLimits(zone);
    
    if nargout < 2
        % LIM = UTMZONE(ZONE)
        varargout{1} = [latlim lonlim];
    elseif nargout == 2
        % [LATLIM, LONLIM] = UTMZONE(ZONE)
        varargout{1} = latlim;
        varargout{2} = lonlim;
    else
        % [LATLIM, LONLIM, MSG] = UTMZONE(ZONE)
        varargout{1} = latlim;
        varargout{2} = lonlim;
        varargout{3} = '';
        warnObsoleteMSGSyntax
    end
else
    nargoutchk(0, 2)
    if nargin == 2
        % UTMZONE(LAT,LON)
        lat = varargin{1};
        lon = varargin{2};
    else
        % UTMZONE(MAT)
        mat = varargin{1};
        map.internal.assert(size(mat,2) == 2, ...
            'map:utm:expectedLatLonColumns','MAT')
        lat = mat(:,1);
        lon = mat(:,2);
    end
    
    if nargout < 2
        % ZONE = UTMZONE(...)
        varargout{1} = findZone(lat, lon);
    else
        % [ZONE, MSG] = UTMZONE(...)
        varargout{1} = findZone(lat, lon);
        varargout{2} = '';
        warnObsoleteMSGSyntax
    end
end
end
%--------------------------------------------------------------------------

function zone = findZone(lat, lon)

checklatlon(lat, lon, 'UTMZONE', 'LAT', 'LON', 1, 2)

% Use geographic mean if lat and lon are non-scalar.
if ~isscalar(lat)
    lat = lat(~isnan(lat));
    lon = lon(~isnan(lon));
    [lat, lon] = meanm(lat, lon);
end

inBounds = (-80 <= lat && lat <= 84) ...
    && (-180 <= lon && lon <= 180);

if inBounds
    lts = [-80:8:72 84]';
    lns = (-180:6:180)';
    latzones = char([67:72 74:78 80:88]');
    
    indx = find(lts <= lat);
    ltindx = indx(max(indx));
    
    indx = find(lns <= lon);
    lnindx = indx(max(indx));
    
    if ltindx < 1 || ltindx > 21
        ltindx = [];
    elseif ltindx == 21
        ltindx = 20;
    end
    
    if lnindx < 1 || lnindx > 61
        lnindx = [];
    elseif lnindx == 61
        lnindx = 60;
    end
    
    if lnindx>31 && lnindx<37 && ~mod(lnindx,2) && ltindx==20
        %on Svalbard?
        switch lnindx
            case 32
                if lon < 9
                    lnindx = 31;
                else
                    lnindx = 33;
                end
            case 34
                if lon < 21
                    lnindx = 33;
                else
                    lnindx = 35;
                end
            case 36
                if lon < 33
                    lnindx = 35;
                else
                    lnindx = 37;
                end
        end
    elseif lnindx==31 && ltindx==18 &&  lon > 3 %on Norway?
        lnindx = 32;
    end
    zone = [num2str(lnindx) latzones(ltindx)];
else
    error(message('map:utm:outsideLimits'))
end

%--------------------------------------------------------------------------
end
function [latlim, lonlim] = zoneLimits(zone)
% The input, ZONE, is a UTM zone string.

if ischar(zone) && 1 <= numel(zone) && numel(zone) <= 3
    latzones = char([67:72 74:78 80:88]');
    switch(numel(zone))
        case 1
            lnindx = str2double(zone);
            ltindx = nan;
        case 2
            num = str2double(zone);
            if isnan(num)
                lnindx = str2double(zone(1));
                ltindx = find(zone(2) == latzones);
            else
                lnindx = num;
                ltindx = nan;
            end
        case 3
            lnindx = str2double(zone(1:2));
            ltindx = find(zone(3) == latzones);
    end
    
    if ~isempty(ltindx) && ~isempty(lnindx) ...
            && 1 <= lnindx && lnindx <= 60
        latlims = [(-80:8:64)' (-72:8:72)'; 72 84];
        lonlims = [(-180:6:174)' (-174:6:180)'];
        
        if isnan(ltindx)
            latlim = [-80 84];
        else
            latlim = latlims(ltindx,:);
        end
        lonlim = lonlims(lnindx,:);
    else
        error(message('map:utm:invalidZoneDesignation', zone))
    end
else
    error(message('map:utm:invalidZoneDesignation', zone))
end
end
%--------------------------------------------------------------------------

function warnObsoleteMSGSyntax

warning(message('map:removed:messageStringOutput',...
    'UTMZONE', 'MSG', 'MSG', 'UTMZONE', 'UTMZONE'))
end

